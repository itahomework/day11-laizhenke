## Daily Report(07/24)

- O: Reviewed the knowledge of HTML, CSS, and JavaScript, learned a new concept of SEO, learned React and React Hook.
- R: puzzled
- I: The reasonable use of HTML tags helps optimize SEO. Previously, I only used Vue for development. Through today's learning, the biggest difference between Vue and React is that Vue supports bidirectional data binding, while React supports unidirectional data binding. The value of each component is transmitted back and forth, making people dizzy and disoriented.
- D: Need to spend more time learning React.
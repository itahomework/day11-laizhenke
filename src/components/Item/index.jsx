import './index.css';

const Item = (props) => {

  const {todo} = props
  return (
    <li>
      {todo}
    </li>
  )
}
export default Item;
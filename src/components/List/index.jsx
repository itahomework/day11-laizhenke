import Item from "../Item";
import './index.css'

const List = (props) => {

  const {todoList} = props

  return(
    <ul className="todo-main">
      {
        todoList.map((todo, index) => {
          return <Item todo={todo} key={index}/>
        })
      }
    </ul>
  )
}

export default List;
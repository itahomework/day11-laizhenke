import './index.css'
const Header = (props) => {

  const {addTodo} = props

  const handleKeyUp = (event) => {
    const {keyCode, target} = event
    if(keyCode !== 13)return
    if(target.value.trim() === ''){
      alert('输入不能为空')
      return
    }
    addTodo(target.value.trim())
    target.value = ''
  }
  return (
    <div className="todo-header">
      <input onKeyUp={handleKeyUp} type="text" placeholder="请输入你的任务名称，按回车键确认" />
    </div>
  )
}
export default Header;